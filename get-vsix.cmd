@echo off
REM download an extension directly from the Marketplace https://marketplace.visualstudio.com/VSCode

REM The URL pattern is:
REM http://${publisher}.gallery.vsassets.io/_apis/public/gallery/publisher/${publisher}/extension/${extension name}/${version}/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage
REM for example eslint:
REM http://dbaeumer.gallery.vsassets.io/_apis/public/gallery/publisher/dbaeumer/extension/vscode-eslint/0.10.18/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage
REM desired output: ${publisher}./${extension name}-${version}.vsix

set publisher=%1
set extension=%2
set version=%3

echo http://%publisher%.gallery.vsassets.io/_apis/public/gallery/publisher/%publisher%/extension/%extension%/%version%/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage
wget http://%publisher%.gallery.vsassets.io/_apis/public/gallery/publisher/%publisher%/extension/%extension%/%version%/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage
ren Microsoft.VisualStudio.Services.VSIXPackage %publisher%.%extension%-%version%.vsix
