@echo off
REM install Visual Code extensions by unzipping extension to UserProfile
set SOURCEDIR=%~dp0\exts
set INSTALLDIR=%USERPROFILE%\.vscode\extensions
set unzip=7z

if "%1" == "" goto :missingExtension
pushd %dp0

if exist "%SOURCEDIR%\%1.zip" set archive=%SOURCEDIR%\%1.zip
if exist "%SOURCEDIR%\%1" set archive=%SOURCEDIR%\%1
if "%archive%" == "" goto :notFound
:found
%unzip% x -o%INSTALLDIR% %archive%
pause
goto :eof

:missingExtension
:notFound
echo Please specify extension archive from %sourcedir%
echo Currently available are:
echo.
pushd %sourcedir%
dir /b
popd
goto :eof
