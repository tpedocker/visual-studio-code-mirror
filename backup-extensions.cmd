@echo off
REM backup currently installed plugins for sharing
REM create zip, but also retrieve vsix package from marketplace
set extdir=%userprofile%\.vscode\extensions
set dstdir=%~dp0\exts
pushd %dstdir%
pushd %extdir%
for /d %%a in ( * ) do (
    REM backup installed extension in %dstdir%
    %~dp0\7z.exe a "%dstdir%\%%~a.zip" "%%~a"
    REM directoryname = publisher.extension-version
    REM get-vsix %%~a
)
popd
REM back in dstdir
popd
REM back in calling directory
