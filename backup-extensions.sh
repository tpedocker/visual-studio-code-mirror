#!/bin/bash
# backup currently installed plugins for sharing
# create zip, but also retrieve vsix package from marketplace
EXT_DIR=~/.vscode/extensions
DST_DIR=~/uploads/vscode-ext

# enable debugging
# set -x

# ensure directories exists
[ -d ~/uploads ] || mkdir ~/uploads
[ -d $DST_DIR ] || mkdir $DST_DIR
pushd $EXT_DIR

for ext in *; do
    # backup installed extension in %DST_DIR%
    zip -r $DST_DIR/${ext}.zip $ext
    #echo directoryname = publisher.extension-version
    #REM get-vsix %%~a
done
popd
#REM back in calling directory
